
const messageItemHelper = {
    // Error validator
    errorName : "name must be at least 5 chars long",
    errorOrdering : "ordering must be a number",
    //Flash Session
    flashUpdate : `Update item success`,
    flashDelete : `Deleted success`,
    flashStatus: `Change status success`,
    flashAdd   : `Add success`,
    //Multiple Flash Session
    multiFlashStatus: ` has been changed success`,
    multtFlashDelete: ` has been deleted`,

}
module.exports = {
 messageItemHelper,

}